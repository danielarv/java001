package deloitte.Academy.lesson01.LogicalOperators;

/**
 * En esta clase se utiliza el operador logico AND para comparar dos numeros con
 * 0
 * 
 * @author dareyes
 *
 */

public class OperadoresLogicos {

	/**
	 * Este metodo compara los valores ingresados con cero
	 * 
	 * @param valor1 El valor debe ser entero
	 * @param valor2 El valor debe ser entero
	 * @return
	 */

	public String operacionAnd(int valor1, int valor2) {

		/**
		 * Aqui se hace la comparacion de ambos numeros con el cero
		 */
		if ((valor1 > 0) && (valor2 > 0)) {
			System.out.println("Ambos numeros son mayores a cero.");
		} else {
			System.out.println("Ingresa numeros mayores a cero.");
		}
		return "Los numeros son mayor a cero.";

	}

}
