package deloitte.Academy.lesson01.Arithmetic;

import java.util.logging.Logger;

/**
 * Crear metodos para operaciones aritmeticas (suma, resta, multiplicacion y division)
 * @author dareyes
 *
 */

public class OperacionesAritmeticas {
	public static final Logger LOGGER = Logger.getLogger(OperacionesAritmeticas.class.getName());

	/*
	 * Metodo para hacer una suma de dos numeros enteros
	 */
	/*
	public int suma(int a, int b) {
		int resultado1 = a + b;
		LOGGER.info("El resultado de la suma es: " + resultado1);
		return resultado1;
	}
	*/

	/*
	 * Metodo para hacer una resta de dos numeros enteros
	 */
	public int resta(int a, int b) {
		int resultado2 = a - b;
		LOGGER.info("El resultado de la resta es: " + resultado2);
		return resultado2;
	}

	/*
	 * Metodo para hacer una multiplicacion de dos numeros enteros
	 */
	public int multi(int a, int b) {
		int resultado3 = a * b;
		LOGGER.info("El resultado de la multiplicacion es: " + resultado3);
		return resultado3;
	}

	/*
	 * Metodo para hacer una division de dos numeros enteros
	 */
	public int division(int a, int b) {
		int resultado4 = a / b;
		LOGGER.info("El resultado de la division es: " + resultado4);
		return resultado4;
	}
}
