package deloitte.Academy.lesson01.Comparisons;

import java.util.logging.Logger;

/**
 * Esta clase crea un metodo para comparar un numero con el 18 y saber si se es
 * mayor/menor de edad
 * 
 * @author dareyes
 *
 */

public class Comparadores {

	/**
	 * La clase LOGGER se utiliza para imprimir informacion con fecha y hora
	 */
	public static final Logger LOGGER = Logger.getLogger(Comparadores.class.getName());

	/**
	 * Esta clase EDAD requiere de un parametro de tipo entero
	 * 
	 * @param a variable de tipo entero que sera comparada con 18
	 * @return
	 */
	public int edad(int a) {
		try {

			/*
			 * Comparacion de un numero con 18, IMPRIME si se es mayor/menor de edad
			 */

			if (a >= 18) {
				LOGGER.info("Eres mayor de edad.");
			} else {
				LOGGER.info("Eres menor de edad.");
			}

		} catch (Exception e) {

			/**
			 * Si no se ingresan numeros enteros, se IMPRIME esta linea
			 */

			System.out.println("ERROR: No ingresaste numeros enteros");

		}
		return a;
	}
}
