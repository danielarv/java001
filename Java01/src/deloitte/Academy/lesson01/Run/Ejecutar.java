package deloitte.Academy.lesson01.Run;

import deloitte.Academy.lesson01.Arithmetic.OperacionesAritmeticas;
import deloitte.Academy.lesson01.Comparisons.Comparadores;
import deloitte.Academy.lesson01.LogicalOperators.OperadoresLogicos;

/**
 * Clase Main para ejecutar los metodos de las clases de operaciones
 * aritmeticas, comparadores y operadores logicos
 * 
 * @author dareyes
 *
 */

public class Ejecutar {

	public static void main(String[] args) {

		/*
		 * Mandar llamar los metodos de la clase de Operaciones aritmeticas
		 */

		OperacionesAritmeticas operacionesAritmeticas = new OperacionesAritmeticas();
		operacionesAritmeticas.suma(5, 1);
		operacionesAritmeticas.resta(5, 3);
		operacionesAritmeticas.multi(2, 2);
		operacionesAritmeticas.division(10, 5);

		/*
		 * Mandar llamar los metodos de la clase de Comparadores
		 */

		Comparadores comparadores = new Comparadores();
		comparadores.edad(16);

		/*
		 * Mandar llamar los metodos de la clase de Operadores logicos
		 */

		OperadoresLogicos operadoresLogicos = new OperadoresLogicos();
		operadoresLogicos.operacionAnd(4, 0);

	}

}
